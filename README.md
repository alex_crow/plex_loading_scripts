# rip_video_disc_to_image

This repository contains Bash scripts used to create  video files for use with video streaming server software such as Jellyfin or Plex. Specifically, this series of scripts is used to rip and convert the contents of Bluray movie discs to .mp4 files, which are in a lossy, but smaller, more suitable format for streaming.

### Dependencies

*  MakeMKV

    - https://www.makemkv.com/forum/viewtopic.php?f=3&t=224
*  MKVToolNix

    `sudo apt install mkvtoolnix`
*  Flatpak

    - https://www.flatpak.org/setup/
*  HandBrakeCLI

    `sudo apt install handbrake-cli` 
