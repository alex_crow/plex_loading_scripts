#!/bin/bash

INPUT_DIR=${HOME}/Videos/movies/raw_movies
INPUT_FILE=${1}
OUTPUT_DIR=${HOME}/Videos/movies/stream_movies
OUTPUT_FILE=${INPUT_FILE::-4}.mp4
SUBTITLE_DIR=${HOME}/Videos/movies/subtitles
SUBTITLE_FILE=${3}

HandBrakeCLI                      \
  -i ${INPUT_DIR}/${INPUT_FILE}   \
  -o ${OUTPUT_DIR}/${OUTPUT_FILE} \
  --optimize                      \
  --srt-file ${SUBTITLE_DIR}/${SUBTITLE_FILE}
