#!/bin/bash

INPUT_DIR=${HOME}/Videos/movies/raw_movies
INPUT_FILE=${1}
OUTPUT_DIR=${HOME}/Videos/movies/stream_movies
OUTPUT_FILE=${INPUT_FILE::-4}.mp4

flatpak run --command=HandBrakeCLI fr.handbrake.ghb \
  --optimize                                        \
  --encoder "nvenc_h265"                            \
  --encoder-preset "slow"                           \
  -i ${INPUT_DIR}/${INPUT_FILE}                     \
  -o ${OUTPUT_DIR}/${OUTPUT_FILE} 
