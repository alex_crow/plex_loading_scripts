#!/bin/bash

INPUT_MOVIE_DIR=${HOME}/Videos/movies/raw_movies
INPUT_MOVIE_FILE=${1}
OUTPUT_SUBTITLE_DIR=${HOME}/Videos/movies/subtitles/raw_subtitles
OUTPUT_SUBTITLE_FILE=${INPUT_MOVIE_FILE::-4}.sup

SUBTITLE_TRACK=$(mkvmerge -i ${INPUT_MOVIE_DIR}/${INPUT_MOVIE_FILE} | grep -i subtitles | head -n 1 | grep -o -E '[0-9]+')

mkvextract tracks                        \
  ${INPUT_MOVIE_DIR}/${INPUT_MOVIE_FILE} \
  ${SUBTITLE_TRACK}:${OUTPUT_SUBTITLE_DIR}/${OUTPUT_SUBTITLE_FILE}
