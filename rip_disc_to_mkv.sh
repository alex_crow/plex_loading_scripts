#!/bin/bash

MOVIE_NAME=${1}

RAW_MOVIE_DIR=${HOME}/Videos/movies/raw_movies

makemkvcon mkv disc:0 all ${RAW_MOVIE_DIR}

mv "${RAW_MOVIE_DIR}/title_t00.mkv" "${RAW_MOVIE_DIR}/${MOVIE_NAME}.mkv"
